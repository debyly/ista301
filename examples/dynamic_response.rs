extern crate env_logger;
#[macro_use]
extern crate log;

extern crate simple_server;

use std::fs;
use simple_server::{Method, Server, StatusCode};

fn main() {
    let host = "127.0.0.1";
    let port = "7878";

    let server = Server::new(|request, mut response| {
        info!("Request received. {} {}", request.method(), request.uri());

        match request.method() {
            &Method::GET => {
                let pth =  request.uri().path();
                println!("{}",pth);
                let audio_dat = vec!['c' as u8];
                if pth.contains(".wasm") {
                    response.header("Content-type","application/wasm");
                    let wasm_file = fs::read(format!(".{}",pth)).unwrap();
                    return Ok(response.body(wasm_file)?)
                }
                if pth.contains(".js") {
                    response.header("Content-type","application/javascript");
                    let wasm_file = fs::read(format!(".{}",pth)).unwrap();
                    return Ok(response.body(wasm_file)?)
                }
                if pth.contains("mp3") || pth.contains("wav") || pth.contains("ogg") {
                    response.header("Content-type","audio/ogg; charset-utf8");
                    let audio_dat = fs::read(format!(".{}",pth)).unwrap();
                    return Ok(response.body(audio_dat)?)
                }
                else{
                    response.header("Content-type","text/html");
                    let hella_instructions = fs::read("./hello.html").unwrap();
                    return Ok(response.body(hella_instructions)?)
                }
            }
            &Method::POST => {
                let data = String::from_utf8_lossy(request.body()).into_owned();
                let body = format!("The data you posted was '{}'", data);
                Ok(response.body(body.into_bytes())?)
            }
            _ => {
                response.status(StatusCode::NOT_FOUND);
                Ok(response.body(b"<h1>404</h1><p>Not found!<p>".to_vec())?)
            }
        }
    });

    server.listen(host, port);
}
